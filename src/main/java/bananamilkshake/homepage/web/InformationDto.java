package bananamilkshake.homepage.web;

import lombok.Value;

@Value
public class InformationDto {

    private final String fullInfo;

    private final String shortInfo;
    private final String realName;
    private final String name;
    private final String email;

    private final String linkedin;
    private final String bitbucket;
    private final String github;
}
