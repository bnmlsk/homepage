package bananamilkshake.homepage.domain;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("information")
public class Information {

	private ObjectId id;
	private int number;

	private String fullInfo;

	private String shortInfo;
	private String realName;
	private String name;
	private String email;

	private String linkedin;
	private String bitbucket;
	private String github;
}
