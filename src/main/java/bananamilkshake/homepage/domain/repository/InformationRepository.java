package bananamilkshake.homepage.domain.repository;

import bananamilkshake.homepage.domain.Information;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InformationRepository extends MongoRepository<Information, Integer> {

    Optional<Information> findByNumber(int number);
}
