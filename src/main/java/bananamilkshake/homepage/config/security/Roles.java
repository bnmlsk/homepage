/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.config.security;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.LoggerFactory;

public interface Roles {
	String ADMIN = "ADMIN";
	
	List<String> ALL = StringFieldsCollector.collect(Roles.class);
	
	/**
	 * Collect all static final string fields values in one collection.
	 */
	class StringFieldsCollector {
		static List<String> collect(Class clazz) {
			List<String> stringVals = new ArrayList<>();
			
			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {				
				if (Modifier.isStatic(field.getModifiers()) && field.getType().equals(String.class)) {
					try {
						stringVals.add(field.get(null).toString());
					} catch (IllegalArgumentException | IllegalAccessException ex) {
						LoggerFactory.getLogger(StringFieldsCollector.class).error("Error on Roles list fill", ex);
					}
				}
			}
			
			return stringVals;
		}
	}
}
