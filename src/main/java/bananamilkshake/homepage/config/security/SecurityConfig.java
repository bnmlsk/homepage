/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.config.security;

import bananamilkshake.homepage.domain.repository.PrincipalsRepository;
import bananamilkshake.homepage.service.security.UserDetailsServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

@Slf4j
@AllArgsConstructor
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final PrincipalsRepository principalsRepository;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		log.info("Create WebSecurityConfigurerAdapter");
		http
			.cors().disable() // no need to use it as only application UI can access it in K8 environment
			.csrf().disable()
			.authorizeRequests()
				.antMatchers("/admin/**").hasRole(Roles.ADMIN)
				.antMatchers("/**").permitAll()
				.and()
			.exceptionHandling()
				.and()
			.formLogin()
				.permitAll()
				.successHandler((request, response, authentication) -> { })
				.failureHandler((request, response, exception) -> response.setStatus(HttpStatus.BAD_REQUEST.value()))
				.and()
			.logout()
				.logoutSuccessHandler((request, response, authentication) -> { })
				.invalidateHttpSession(true)
				.permitAll()
				.and()
			.rememberMe()
				.tokenValiditySeconds(30 * 60); // 30 min
	}

	@Autowired
	protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		log.info("Configure global security");
		
		auth
				.userDetailsService(userDetailsService())
				.passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new SCryptPasswordEncoder();
	}

	/* Responsible for retrieving principal from application repository by his username and password. */
	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		return new UserDetailsServiceImpl(principalsRepository);
	}
}
