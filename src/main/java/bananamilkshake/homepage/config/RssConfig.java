package bananamilkshake.homepage.config;

import bananamilkshake.homepage.rss.RssViewer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.BeanNameViewResolver;

@Configuration
public class RssConfig {

	@Bean
	public RssViewer rssViewer() {
		return new RssViewer();
	}

	@Bean
	public BeanNameViewResolver beanNameViewResolver() {
		return new BeanNameViewResolver();
	}
}
