package bananamilkshake.homepage.config;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.time.Duration;
import java.util.Properties;

@Slf4j
@Setter
@Configuration
@ConfigurationProperties(prefix = "homepage.mail")
public class MailConfiguration {

	private String host;
	private int port;
	private String username;
	private String password;
	private Duration connectionTimeout;

	@Bean
	public JavaMailSender javaMailSender() {
		Properties prop = new Properties();
		prop.put("mail.smtp.ssl.enable","true");
		prop.put("mail.smtp.connectiontimeout", connectionTimeout.getSeconds());
		prop.put("mail.smtp.timeout", connectionTimeout.getSeconds());
		prop.put("mail.smtp.writetimeout", connectionTimeout.getSeconds());

		JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
		javaMailSenderImpl.setHost(host);
		javaMailSenderImpl.setPort(port);
		javaMailSenderImpl.setUsername(username);
		javaMailSenderImpl.setPassword(password);
		javaMailSenderImpl.setJavaMailProperties(prop);

		return javaMailSenderImpl;
	}

	@Bean
	public VelocityEngine velocityEngine() {
		VelocityEngine engine = new VelocityEngine();
		engine.addProperty("resource.loader", "class");
		engine.addProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		return engine;
	}
}
