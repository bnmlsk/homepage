/*
 * Copyright (C) 2016 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package bananamilkshake.homepage.service;

import bananamilkshake.homepage.domain.Project;
import bananamilkshake.homepage.domain.repository.ProjectsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ProjectsService {

	@Autowired
	private ProjectsRepository projectsRepository;
	
	public Project get(String link) {
		return projectsRepository.findByLink(link);
	}
	
	public List<Project> getProjects() {
		return projectsRepository.findAll();
	}

	@Secured("ROLE_ADMIN")
	public Project addProject(Project project) {
		log.info("Creating new project {}", project);
		return projectsRepository.save(project);
	}

	@Secured("ROLE_ADMIN")
	public Project editProject(Project newProject) {
		log.info("Edit project {}", newProject);
		Project project = projectsRepository.findById(newProject.getId()).orElseThrow();
		project.setName(newProject.getName());
		project.setDescription(newProject.getDescription());
		return projectsRepository.save(project);
	}
}
