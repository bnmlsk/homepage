/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.service;

import bananamilkshake.homepage.exceptions.NonExistentArticleException;
import bananamilkshake.homepage.domain.Article;
import bananamilkshake.homepage.domain.repository.ArticlesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Slf4j
@Service
public class ArticlesService {

	private static final Sort ARTICLES_SORT = Sort.by(Sort.Direction.DESC, "creationDate");

	private final ArticlesRepository articlesRepository;
	private final Integer blockSize;

	public ArticlesService(ArticlesRepository articlesRepository,
						   @Value("${homepage.articles.block-size}") Integer blockSize) {
		this.articlesRepository = articlesRepository;
		this.blockSize = blockSize;
	}

	/**
	 * Retrieves record with link from repository and returns it. If no
	 * record with such id exists throws {@link NonExistentArticleException} exception.
	 *
	 * @return article with link
	 */
	public Article getArticle(String link) {
		Article article = articlesRepository.findOneByLinkAndPublishedTrue(link);
		if (article == null) {
			log.warn("No article with link {} found", link);
			throw new NonExistentArticleException(link);
		}
		
		return article;
	}

	public Page<Article> getPage(Integer block) {
		Pageable pageRequest = PageRequest.of(block, blockSize, ARTICLES_SORT);
		return articlesRepository.findByPublishedTrue(pageRequest);
	}

	public List<Article> getAllPublished() {
		return articlesRepository.findByPublishedTrueOrderByCreationDateDesc();
	}

	public Page<Article> getPageWithTag(String tag, Integer block) {
		Pageable pageRequest = PageRequest.of(block, blockSize, ARTICLES_SORT);
		return articlesRepository.findByTagsAndPublishedTrue(tag, pageRequest);
	}

	@Secured("ROLE_ADMIN")
	public Page<Article> getAllPage(int block) {
		Pageable pageRequest = PageRequest.of(block, blockSize, ARTICLES_SORT);
		return articlesRepository.findAll(pageRequest);
	}

	@Secured("ROLE_ADMIN")
	public Article add(Article article) {
		log.info("Add new article {}", article);
		article.setCreationDate(LocalDateTime.now(ZoneId.of("UTC")));
		return articlesRepository.save(article);
	}

	@Secured("ROLE_ADMIN")
	public Article edit(Article newArticle) {
		Article article = articlesRepository.findById(newArticle.getId()).orElseThrow();
		newArticle.setId(article.getId());
		log.info("Update article {}", newArticle);
		return articlesRepository.save(newArticle);
	}
}
