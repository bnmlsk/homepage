/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.service.security;

import bananamilkshake.homepage.domain.Principal;
import bananamilkshake.homepage.domain.repository.PrincipalsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

@Slf4j
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

	private final PrincipalsRepository principalsRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Principal principal = principalsRepository.findByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException(format("No user with name '%s'", username)));

		log.debug("Load user directly from datasource: username {}, roles {}", principal.getUsername(), principal.getAuthorities());
		
		return createUserDetailsFromPrincipal(principal);
	}

	private UserDetails createUserDetailsFromPrincipal(Principal principal) {
		return new User(principal.getUsername(),
			principal.getPassword(),
			true,
			true,
			true,
			true,
			extractAuthorities(principal));
	}

	static private List<SimpleGrantedAuthority> extractAuthorities(Principal principal) {
		return principal.getAuthorities()
				.stream()
				.map(SimpleGrantedAuthority::new)
				.collect(toList());
	}
}
