package bananamilkshake.homepage.service;

import bananamilkshake.homepage.config.security.Roles;
import bananamilkshake.homepage.domain.Information;
import bananamilkshake.homepage.domain.repository.InformationRepository;
import bananamilkshake.homepage.exceptions.NonExistentInformationException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class InformationService {

	private final int id;
	private final InformationRepository informationRepository;

	public InformationService(@Value("${homepage.information.id}") int number,
							  InformationRepository informationRepository) {
		this.id = number;
		this.informationRepository = informationRepository;
	}

	public Information getInfo() {
		return informationRepository.findByNumber(id).orElseThrow(NonExistentInformationException::new);
	}

	@Secured("ROLE_ADMIN")
	public Information update(Information information) {
		Information existing = informationRepository.findByNumber(id).orElseThrow();
		information.setNumber(id);
		information.setId(existing.getId());
		return informationRepository.save(information);
	}
}
