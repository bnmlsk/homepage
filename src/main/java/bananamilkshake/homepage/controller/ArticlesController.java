/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.domain.Article;
import bananamilkshake.homepage.service.ArticlesService;
import bananamilkshake.homepage.web.ArticleDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/articles")
@AllArgsConstructor
public class ArticlesController {

	private final ArticlesService articlesService;
	private final MapperFacade mapperFacade;

	@GetMapping
	public Page<ArticleDto> getPage(@RequestParam(defaultValue = "1") Integer pageId) {
		return mapPage(articlesService.getPage(pageId - 1));
	}

	@GetMapping("/{link}")
	public ArticleDto getArticle(@PathVariable String link) {
		return mapperFacade.map(articlesService.getArticle(link), ArticleDto.class);
	}

	@GetMapping("/search")
	public Page<ArticleDto> getPageByTag(@RequestParam String tag, @RequestParam(defaultValue = "1") Integer pageId) {
		return mapPage(articlesService.getPageWithTag(tag, pageId - 1));
	}

	private PageImpl<ArticleDto> mapPage(Page<Article> page) {
		return new PageImpl<>(mapperFacade.mapAsList(page.getContent(), ArticleDto.class), page.getPageable(), page.getTotalElements());
	}
}
