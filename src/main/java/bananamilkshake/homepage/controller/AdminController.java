/*
 * Copyright (C) 2015 Liza Lukicheva
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.domain.Article;
import bananamilkshake.homepage.domain.Information;
import bananamilkshake.homepage.domain.Project;
import bananamilkshake.homepage.service.ArticlesService;
import bananamilkshake.homepage.service.InformationService;
import bananamilkshake.homepage.service.ProjectsService;
import bananamilkshake.homepage.web.ArticleDto;
import bananamilkshake.homepage.web.InformationDto;
import bananamilkshake.homepage.web.ProjectDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/admin")
@AllArgsConstructor
public class AdminController {

    private final ArticlesService articlesService;
    private final ProjectsService projectsService;
    private final InformationService informationService;
    private final MapperFacade mapperFacade;

    @PutMapping("/information")
    public InformationDto update(@RequestBody InformationDto information) {
        return mapperFacade.map(informationService.update(mapperFacade.map(information, Information.class)), InformationDto.class);
    }

    @GetMapping("/articles")
    public Page<ArticleDto> getAll(@RequestParam(defaultValue = "1") Integer pageId) {
        return mapPage(articlesService.getAllPage(pageId - 1));
    }

    @PostMapping("/articles")
    public ResponseEntity<?> createArticle(@RequestBody ArticleDto articleDto) {
		articlesService.add(mapperFacade.map(articleDto, Article.class));
		return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/articles")
    public ArticleDto editArticle(@RequestBody ArticleDto articleDto) {
        return mapperFacade.map(articlesService.edit(mapperFacade.map(articleDto, Article.class)), ArticleDto.class);
    }

    @PostMapping("/projects")
    public ResponseEntity<?> createProject(@RequestBody ProjectDto projectDto) {
		projectsService.addProject(mapperFacade.map(projectDto, Project.class));
		return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/projects")
    public ProjectDto editProject(@RequestBody ProjectDto projectDto) {
        Project project = mapperFacade.map(projectDto, Project.class);
        Project updatedProject = projectsService.editProject(project);
        return mapperFacade.map(updatedProject, ProjectDto.class);
    }

    private PageImpl<ArticleDto> mapPage(Page<Article> page) {
        return new PageImpl<>(mapperFacade.mapAsList(page.getContent(), ArticleDto.class), page.getPageable(), page.getTotalElements());
    }
}
