package bananamilkshake.homepage.controller;

import bananamilkshake.homepage.service.InformationService;
import bananamilkshake.homepage.web.InformationDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/information")
@AllArgsConstructor
public class InformationController {

	private final InformationService informationService;
	private final MapperFacade mapperFacade;

	@GetMapping
	public InformationDto getInformation() {
		return mapperFacade.map(informationService.getInfo(), InformationDto.class);
	}
}
