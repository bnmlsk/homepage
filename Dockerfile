FROM openjdk:12
WORKDIR /app
COPY target/homepage-*.jar /app/homepage.jar
EXPOSE 8080
CMD ["java", "-jar", "/app/homepage.jar"]