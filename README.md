# README

## What is this repository for?

This is my simple homepage. You can check it out in [bananamilkshake.me](https://bananamilkshake.me/). It can contains description about me, articles, projects descriptions. It also have an admin page where I can edit all this stuff.

## How do I get set up?

### Summary of set up
For application server deployment create war file using ``gradle war`` command.
To deploy project directly to Wildfly application server task ``gradle deploy`` can be used.

### Database configuration

Postgres database can be deployed to K8 using deployment/deployment.yaml configuration.
It will prepare a service named `homepage-postgres`. In order to enable connection to it
use command to expose a port on localhost:
```
kubectl port-forward service/homepage-postgres 5432:5432
```

Connect to pod to apply test data with a command:
```
kubectl exec -it homepage-postgres -- /bin/bash
```

and execute data upload to postgres:
```
psql -U postgres -d postgres -a -f /etc/test-data.sql
```

#### Schema preparation

Project uses [liquibase](http://www.liquibase.org/) to perform database migrations. Schema migration runs automatically
on service startup. Path to migrations is `src/main/resources/db`.

### JSM configuration ###

In order to send notification e-mails it is necessary to configure JMS. Application requires JMS queue with name ``HomepageMailQueue`` (jndi value ``java:/jms/HomepageMailQueue``) and JSM connection factory with jndi ``java:/jms/HomepageConnectionFactory``.

### System properties

```
#!

homepage.information.id					-- record id in 'information' table to use for information data
homepage.information.cacheExpiration	-- minutes to wait until cached information data is expired
homepage.jms.connectionsCacheSize		-- jms session cache size ()
homepage.mail.connectionTimeout			-- value for smtp mail.smtp.connectiontimeout parameter
homepage.mail.from						-- email to send notification messages
homepage.mail.username					-- login for mail
homepage.mail.password					-- mailbox password
homepage.mail.host						-- mail provider out host
homepage.mail.port						-- mail provider out port
```

## Author

If you have any questions about project feel free to write me `me@bananamilkshake.me`.