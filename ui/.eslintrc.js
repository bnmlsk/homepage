module.exports = {
  'env': {
    'browser': true,
    'es6': true,
    'node': true,
  },
  'extends': [
    'plugin:react/recommended',
    'google',
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
    },
    'ecmaVersion': 2018,
    'sourceType': 'module',
  },
  'plugins': [
    'react',
  ],
  'rules': {
    'react/prop-types': 0,
    'react/jsx-first-prop-new-line': ['error', 'multiline'],
    'react/jsx-indent-props': ['error', 'first'],
    'max-len': ['error', 120, {
      'ignoreUrls': true
    }],
    'require-jsdoc': ['off'],
    'indent': ['error', 2, {
      'VariableDeclarator': 2,
      'MemberExpression': 1,
      'FunctionDeclaration': {'parameters': 'first'},
      'FunctionExpression': {'parameters': 'first'},
      'CallExpression': {'arguments': 'first'},
      'ArrayExpression': 'first',
      'ObjectExpression': 'first',
      'ImportDeclaration': 'first'
      }
    ],
  },
  'settings': {
    'react': {
      'version': 'detect'
    }
  }
};
