import React from 'react';
import moment from 'moment';

export default function Date({date}) {
  const style = {
    color: '#a6a6a6',
    fontSize: 'x-small',
  };

  return (
    <div style={style}>
      { moment(date).format('LL') }
    </div>
  );
}
