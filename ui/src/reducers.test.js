import reducers from './reducers';
import * as homepage from './homepage/actions';
import * as admin from './admin/actions';

describe('reducers', () => {
  describe('homepage', function() {
    describe('choosePage', () => {
      describe('choosePage', () => {
        it('should handle current page change', () => {
          const state = reducers({})({currentPage: 'INFORMATION'}, homepage.choosePage('ARTICLES'));
          expect(state.currentPage).toEqual('ARTICLES');
        });
      });
    });

    describe('information', () => {
      describe('setInformation', function() {
        it('should handle information setup', () => {
          const state = reducers({})({information: {}}, homepage.setInformation({
            fullInfo: 'Full information description',
            shortInfo: 'Short info',
            realName: 'John Smith',
            name: 'johnsmith',
            email: 'me@test.com',
            linkedin: 'http://linkedin.com',
            bitbucket: 'http://bitbucket.org',
            github: 'http://github.com',
          }));

          expect(state.information).toEqual({
            fullInfo: 'Full information description',
            shortInfo: 'Short info',
            realName: 'John Smith',
            name: 'johnsmith',
            email: 'me@test.com',
            linkedin: 'http://linkedin.com',
            bitbucket: 'http://bitbucket.org',
            github: 'http://github.com',
          });
        });
      });
    });

    describe('articles', () => {
      const articles = [
        {
          id: '123',
          link: 'link1',
          title: 'Article 1',
          text: 'text1',
          tags: ['Linux'],
          creationDate: '2020-03-02T20:58:31.005',
          published: true,
        },
        {
          id: '456',
          link: 'link2',
          title: 'Article 2',
          text: 'text2',
          tags: ['Linux', 'C++', 'Fun', 'Some tag'],
          creationDate: '2012-03-12T11:12:13',
          published: true,
        },
      ];

      describe('setArticles', () => {
        it('should set articles page information', () => {
          const number = 1;
          const totalPages = 3;

          const state = reducers({})({articles: {}}, homepage.setArticles(articles, number, totalPages));

          expect(state.articles).toEqual({
            content: articles,
            pageId: 2,
            totalPages: 3,
            tag: null,
            opened: null,
          });
        });

        it('should set articles page information and searched tag when provided', () => {
          const number = 1;
          const totalPages = 3;
          const tag = 'Some tag';

          const state = reducers({})({articles: {}}, homepage.setArticles(articles, number, totalPages, tag));

          expect(state.articles).toEqual({
            content: articles,
            pageId: 2,
            totalPages: 3,
            tag: 'Some tag',
            opened: null,
          });
        });
      });

      describe('openArticle', () => {
        it('should set opened article', () => {
          const article = {
            id: '789',
            link: 'link3',
            title: 'Article 3',
            text: 'text3',
            tags: ['Linux'],
            creationDate: '2012-03-12T11:12:13',
            published: true,
          };

          const state = reducers({})({articles: {
            content: articles,
            pageId: 2,
            totalPages: 3,
            tag: null,
            opened: null}},
                                     homepage.openArticle(article));

          expect(state.articles).toEqual({
            content: articles,
            pageId: 2,
            totalPages: 3,
            tag: null,
            opened: article,
          });
        });
      });

      describe('articleNotFound', () => {
        it('should set opened article as not found', () => {
          const state = reducers({})({articles: {
            content: articles,
            pageId: 2,
            totalPages: 3,
            tag: null,
            opened: null}}, homepage.articleNotFound());

          expect(state.articles).toEqual({
            content: articles,
            pageId: 2,
            totalPages: 3,
            tag: null,
            opened: 'Not found',
          });
        });
      });
    });

    describe('projects', function() {
      const projects = [
        {link: 'project-1', name: 'Project 1'},
        {link: 'project-2', name: 'Project 2'},
      ];

      describe('setProjects', function() {
        it('should set all projects', function() {
          const state = reducers({})({projects: {content: [], activeIndex: 0}},
                                     homepage.setProjects(projects, 2));

          expect(state.projects).toEqual({
            content: projects,
            activeIndex: 2,
          });
        });
      });

      describe('setActiveProject', () => {
        it('should select active project', function() {
          const state = reducers({})({projects: {content: projects, activeIndex: 0}},
                                     homepage.setActiveProject(1));

          expect(state.projects).toEqual({
            content: projects,
            activeIndex: 1,
          });
        });
      });
    });
  });
  describe('admin', function() {
    describe('editInformation', function() {
      const information = {
        name: 'John Smith',
        fullInfo: 'Full Information',
      };

      describe('setInformation', () => {
        it('should set loaded information', () => {
          const state = reducers({})({}, admin.setInformation(information));
          expect(state.editInformation).toEqual({current: information, changed: information});
        });
      });
      describe('changeInformation', () => {
        it('should update information details', () => {
          const state = reducers({})({editInformation: { current: information, changed: information}},
                                     admin.changeInformation({fullName: 'Some full name'}));
          expect(state.editInformation).toEqual({current: information, changed: {
            name: 'John Smith',
            fullInfo: 'Full Information',
            fullName: 'Some full name'}});
        });
      });
    });
  });
});
