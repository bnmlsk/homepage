import React from 'react';
import 'semantic-ui-css/semantic.min.css';
import {Label} from 'semantic-ui-react';
import {Link} from 'react-router-dom';

export default function Tags({tags}) {
  return (
    <Label.Group size='tiny'>
      {(tags || []).map((tag) =>
        <Label key={tag}>
          <Link to={'/articles/search?tag=' + tag}>{tag}</Link>
        </Label>,
      )}
    </Label.Group>
  );
}
