import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';

import currentPage from './homepage/reducers/currentPage';
import information from './homepage/reducers/information';
import articles from './homepage/reducers/articles';
import projects from './homepage/reducers/projects';

import editInformation from './admin/reducers/editInformation';
import editArticle from './admin/reducers/editArticle';
import editProject from './admin/reducers/editProject';
import message from './admin/reducers/message';

import login from './login/reducers/login';

export default (history) => combineReducers({
  router: connectRouter(history),

  currentPage,
  information,
  articles,
  projects,

  editInformation,
  editArticle,
  editProject,
  message,

  login,
});
