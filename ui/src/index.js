import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {createBrowserHistory} from 'history';
import {ConnectedRouter, routerMiddleware} from 'connected-react-router';
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import rootReducer from './reducers';

import Homepage from './homepage/containers/App';
import Admin from './admin/containers/App';
import Login from './login/containers/Login';

import axios from 'axios';
import {useIsAuthorised} from "./login/reducers/login";

axios.defaults.baseURL = process.env.REACT_APP_BACKEND_URL;
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.withCredentials = true;

export const history = createBrowserHistory();

const composeEnhancers = composeWithDevTools({
  serialize: {
    options: true,
  }
});
const store = createStore(
  rootReducer(history),
  composeEnhancers(applyMiddleware(thunk, logger, routerMiddleware(history))));

function App() {

  const isAuthorised = useIsAuthorised();

  return <ConnectedRouter history={history}>
    <Switch>
      {!isAuthorised && <Route path="/login"><Login /></Route>}
      {isAuthorised && <Route path="/admin"><Admin /></Route>}
      <Route path="/"><Homepage /></Route>
    </Switch>
  </ConnectedRouter>;
}

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
