import {
  closeArticleModal,
  closeProjectModal,
  setArticles,
  setFailure,
  setInformation,
  setProjects,
  setSuccess
} from '../actions';
import axios from 'axios';

export function fetchInformation() {
  return async (dispatch) => {
    const response = await axios.get('/information');
    dispatch(setInformation(response.data));
  }
}

export function editInformation(information) {
  return async (dispatch) => {
    try {
      await axios.put('/admin/information', information);
      dispatch(setSuccess('Information successfully updated'));
    } catch(error) {
      dispatch(setFailure('Information failed to be updated', getMessage(error)));
    }
  }
}

export function fetchAllArticles(location) {
  return async (dispatch) => {
    const response = await axios.get(`/admin/articles${location.search}`);

    const articlesPage = response.data;

    dispatch(setArticles(articlesPage.content, articlesPage.number, articlesPage.totalPages));
  }
}

export function addArticle(newArticle) {
  return async (dispatch) => {
    try {
      await axios.post('/admin/articles', newArticle);
      dispatch(setSuccess(`New article "${newArticle.link}" added`));
      dispatch(closeArticleModal());
    } catch(error) {
      dispatch(setFailure('Failed to add article', getMessage(error)));
    }
  }
}

export function editArticle(changedArticle) {
  return async (dispatch) => {
    try {
      await axios.put('/admin/articles', changedArticle);
      dispatch(setSuccess(`Updated article "${changedArticle.link}"`));
      dispatch(closeArticleModal());
    } catch(error) {
      dispatch(setFailure('Failed to edit article', getMessage(error)));
    }
  }
}

export function fetchAllProjects() {
  return async (dispatch) => {
    const response = await axios.get('/projects');
    dispatch(setProjects(response.data));
  }
}

export function addProject(newProject) {
  return async (dispatch) => {
    try {
      await axios.post('/admin/projects', newProject);
      dispatch(setSuccess(`New project "${newProject.link}" added`));
      dispatch(closeProjectModal());
    } catch(error) {
      dispatch(setFailure('Failed to add project', getMessage(error)));
    }
  }
}

export function editProject(changedProject) {
  return async (dispatch) => {
    try {
      await axios.put('/admin/projects', changedProject);
      dispatch(setSuccess('Updated project "' + changedProject.link + '"'));
      dispatch(closeProjectModal());
    } catch(error) {
      dispatch(setFailure('Failed to edit project', getMessage(error)));
    }
  }
}

function getMessage(error) {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    return error.response.data.message;
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    return error.request;
  } else {
    // Something happened in setting up the request that triggered an Error
    return error.message;
  }
}
