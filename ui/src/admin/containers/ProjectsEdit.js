import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Divider, Message} from 'semantic-ui-react';

import {changeProject,
        openProjectModal,
        closeProjectModal,
        setSuccess,
        dismissSuccess,
        dismissFailure} from '../actions';
import {fetchAllProjects, addProject, editProject} from './fetch';
import ProjectPreview from '../components/ProjectPreview';
import ProjectEditModal from '../components/ProjectEditModal';

function EditModal({label, icon, project={}, onSubmitAction}) {
  const projectChanges = useSelector((state) => state.editProject.projectChanges);
  const failure = useSelector((state) => state.message.failure);

  const dispatch = useDispatch();

  return (
    <ProjectEditModal
      vis={{label, icon, opened: projectChanges != null}}
      failure={failure}
      project={projectChanges}
      handleOpened={(isToOpen) => dispatch(isToOpen ? openProjectModal(project) : closeProjectModal())}
      onSubmit={(project) => dispatch(onSubmitAction(project))}
      onChange={(e, {name, value}) => dispatch(changeProject({[name]: value}))}
      dismissFailure={() => dispatch(dismissFailure())}/>
  );
}

export default function ProjectsEdit() {
  const projects = useSelector((state) => state.editProject.projects);
  const success = useSelector((state) => state.message.success);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllProjects());
  }, [dispatch, success]);

  const projectPreview = (project) => {
    const editModal = (<EditModal label='Edit project' project={project} icon='edit' onSubmitAction={editProject}/>);
    return (<ProjectPreview key={project.link} project={project} setSuccess={setSuccess} editModal={editModal} />);
  };

  return (
    <React.Fragment>
      {success && <Message positive header={success.title} onDismiss={ () => dispatch(dismissSuccess()) }/>}
      <EditModal label='Add project' icon='add' onSubmitAction={addProject} />
      <Divider />
      {projects.map(projectPreview)}
    </React.Fragment>
  );
}
