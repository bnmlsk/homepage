import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useLocation, useHistory} from 'react-router-dom';
import {openArticleModal, closeArticleModal, changeArticle, addTags, dismissSuccess, dismissFailure} from '../actions';
import {Container, Divider, Pagination, Message} from 'semantic-ui-react';
import {fetchAllArticles, addArticle, editArticle} from './fetch';
import ArticleEditModal from '../components/ArticleEditModal';
import ArticlePreview from '../components/ArticlePreview';

function EditModal({label, icon, article={}, onSubmitAction}) {
  const articleChanges = useSelector((state) => state.editArticle.articleChanges);
  const tags = useSelector((state) => state.editArticle.tags);

  const failure = useSelector((state) => state.message.failure);

  const dispatch = useDispatch();

  return (
    <ArticleEditModal
      vis={{label, icon, opened: articleChanges != null}}
      failure={failure}
      tags={tags}
      article={articleChanges}
      handleOpened={(isToOpen) => dispatch(isToOpen ? openArticleModal(article) : closeArticleModal())}
      handleSubmit={(article) => dispatch(onSubmitAction(article))}
      handleChange={(e, {name, value}) => dispatch(changeArticle({[name]: value}))}
      handleNewTag={(e, {value}) => dispatch(addTags([value]))}
      dismissFailure={() => dispatch(dismissFailure())}/>
  );
}

export default function ArticlesEdit() {
  const articles = useSelector((state) => state.editArticle.articles);
  const totalPages = useSelector((state) => state.editArticle.totalPages);
  const pageId = useSelector((state) => state.editArticle.pageId);

  const success = useSelector((state) => state.message.success);

  const dispatch = useDispatch();

  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    dispatch(fetchAllArticles(location));
  }, [dispatch, location, location.search]);

  const onPageChange = (pageId) => history.push(`${history.location.pathname}?pageId=${pageId}`);

  const articlePreview = (article) => {
    const editModal = (<EditModal label='Edit article' icon='edit' article={article} onSubmitAction={editArticle} />);
    return (<ArticlePreview key={article.link} article={article} editModal={editModal} />);
  };

  return (
    <React.Fragment>
      {success && <Message positive header={success.title} onDismiss={ () => dispatch(dismissSuccess()) }/>}
      <EditModal label='Add article' icon='add' onSubmitAction={addArticle} />
      <Divider />
      <Container>
        {articles.map(articlePreview)}
      </Container>
      <Divider hidden />
      <Pagination
        defaultActivePage={pageId}
        totalPages={totalPages}
        boundaryRange={5}
        onPageChange={ (event, data) => onPageChange(data.activePage) } />
    </React.Fragment>
  );
}
