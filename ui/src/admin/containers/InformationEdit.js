import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import {changeInformation, dismissSuccess, dismissFailure} from '../actions';
import {fetchInformation, editInformation} from './fetch';
import InformationFields from '../components/InformationFields';

export default function InformationEdit() {
  const changed = useSelector((state) => state.editInformation.changed);

  const success = useSelector((state) => state.message.success);
  const failure = useSelector((state) => state.message.failure);

  const dispatch = useDispatch();

  const onChange = (e, {name, value}) => dispatch(changeInformation({[name]: value}));

  useEffect(() => { dispatch(fetchInformation()) }, [dispatch]);

  return (
    <InformationFields
      information={changed}
      onChange={onChange}
      success={success}
      dismissSuccess={() => dispatch(dismissSuccess())}
      failure={failure}
      dismissFailure={() => dispatch(dismissFailure())}
      handleUpdate={() => dispatch(editInformation(changed))} />
  );
}
