import {Icon, Menu, Header, Grid, Segment} from 'semantic-ui-react';
import React from 'react';
import {Switch, Route, Link, useRouteMatch} from 'react-router-dom';
import InformationEdit from './InformationEdit';
import ArticlesEdit from './ArticlesEdit';
import ProjectsEdit from './ProjectsEdit';

function Main() {
  return (
    <React.Fragment>
      <Header as='h1'>Greetings!</Header>
      <Header as='h2'>What can I do for you, master?</Header>
    </React.Fragment>
  );
}

export default function App() {
  const {path} = useRouteMatch();

  return (
    <Grid>
      <Grid.Column width={4}>
        <Menu vertical inverted attached fixed='left'>
          <Link to="/">
            <Menu.Item>
              <Icon name="home"/>
                          Homepage
            </Menu.Item>
          </Link>
          <Link to={'/admin/information'}>
            <Menu.Item>
              <Icon name="info"/>
                          Information
            </Menu.Item>
          </Link>
          <Link to={'/admin/articles'}>
            <Menu.Item>
              <Icon name="book"/>
                          Articles
            </Menu.Item>
          </Link>
          <Link to={'/admin/projects'}>
            <Menu.Item>
              <Icon name="laptop"/>
                          Projects
            </Menu.Item>
          </Link>
        </Menu>
      </Grid.Column>
      <Grid.Column width={8}>
        <Segment raised>
          <Switch>
            <Route path={'/admin/information'} component={InformationEdit}/>
            <Route path={'/admin/articles'} component={ArticlesEdit}/>
            <Route path={'/admin/projects'} component={ProjectsEdit}/>
            <Route exact path={path} component={Main} />
          </Switch>
        </Segment>
      </Grid.Column>
    </Grid>
  );
}
