import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import expect from 'expect';

import * as actions from '../actions';
import * as fetchers from './fetch';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('fetch', () => {
  describe('fetchInformation', () => {
    it('should fetch information and send it as a state', () => {
      const information = {
        fullName: 'Test Name',
        shortInfo: 'Test short information',
      };

      nock('http://localhost:8080')
        .get('/information')
        .reply(200, information);

      const expectedActions = [
        actions.setInformation(information),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.fetchInformation()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('editInformation', () => {
    it('should set success if information update succeeded', () => {
      const information = {
        fullName: 'Test Name',
        shortInfo: 'Test short information',
      };

      nock('http://localhost:8080')
        .put('/admin/information')
        .reply(200, information);

      const expectedActions = [
        actions.setSuccess('Information successfully updated'),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.editInformation(information)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should set failure if information update fails', () => {
      const information = {
        fullName: 'Test Name',
        shortInfo: 'Test short information',
      };

      nock('http://localhost:8080')
        .put('/admin/information')
        .reply(400, {message: 'Test failure on information update'});

      const expectedActions = [
        actions.setFailure('Information failed to be updated', 'Test failure on information update'),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.editInformation(information)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchAllArticles', () => {
    it('should fetch all articles pages using position from current location', () => {
      const articles = [
        {title: 'Article 1', link: 'article-1'},
        {title: 'Article 2', link: 'article-2'},
        {title: 'Article 3', link: 'article-3'},
      ];
      const pageId = 2;
      const totalPages = 10;

      nock('http://localhost:8080')
        .get('/admin/articles?pageId=2')
        .reply(200, {
          content: articles,
          number: pageId,
          totalPages: totalPages,
        });

      const expectedActions = [
        actions.setArticles(articles, pageId, totalPages),
      ];

      const store = mockStore({ });

      const location = {search: '?pageId=2'};

      return store.dispatch(fetchers.fetchAllArticles(location)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('addArticle', () => {
    const article = {
      title: 'Test article',
      link: 'article-link',
      text: 'Some test text',
    };

    it('should set success and close modal if article creation is successful', () => {
      nock('http://localhost:8080')
        .post('/admin/articles')
        .reply(201);

      const expectedActions = [
        actions.setSuccess('New article "article-link" added'),
        actions.closeArticleModal(),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.addArticle(article)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should set failure if article creation failed', () => {
      nock('http://localhost:8080')
        .post('/admin/articles')
        .reply(400, {message: 'Test failure to add article'});

      const expectedActions = [
        actions.setFailure('Failed to add article', 'Test failure to add article'),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.addArticle(article)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('editArticle', () => {
    const article = {
      title: 'Test article',
      link: 'article-link',
      text: 'Some test text',
    };

    it('should set success and close modal if article update is successful', () => {
      nock('http://localhost:8080')
        .put('/admin/articles')
        .reply(200);

      const expectedActions = [
        actions.setSuccess('Updated article "article-link"'),
        actions.closeArticleModal(),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.editArticle(article)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should set failure if article update failed', () => {
      nock('http://localhost:8080')
        .put('/admin/articles')
        .reply(400, {message: 'Test failure to update article'});

      const expectedActions = [
        actions.setFailure('Failed to edit article', 'Test failure to update article'),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.editArticle(article)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchProjects', () => {
    it('should fetch all projects', () => {
      const projects = [
        {name: 'Project 1', link: 'project-1'},
        {name: 'Project 2', link: 'project-2'},
        {name: 'Project 3', link: 'project-3'},
      ];

      nock('http://localhost:8080')
        .get('/projects')
        .reply(200, projects);

      const expectedActions = [
        actions.setProjects(projects),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.fetchAllProjects()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('addProject', () => {
    const project = {
      name: 'Test project',
      link: 'project-link',
      description: 'Some test text',
    };

    it('should set success and close modal if project creation is successful', () => {
      nock('http://localhost:8080')
        .post('/admin/projects')
        .reply(201);

      const expectedActions = [
        actions.setSuccess('New project "project-link" added'),
        actions.closeProjectModal(),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.addProject(project)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should set failure if project creation failed', () => {
      nock('http://localhost:8080')
        .post('/admin/projects')
        .reply(400, {message: 'Test failure to add project'});

      const expectedActions = [
        actions.setFailure('Failed to add project', 'Test failure to add project'),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.addProject(project)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('editProject', () => {
    const project = {
      name: 'Test project',
      link: 'project-link',
      description: 'Some test text',
    };

    it('should set success and close modal if project update is successful', () => {
      nock('http://localhost:8080')
        .put('/admin/projects')
        .reply(201);

      const expectedActions = [
        actions.setSuccess('Updated project "project-link"'),
        actions.closeProjectModal(),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.editProject(project)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should set failure if project update failed', () => {
      nock('http://localhost:8080')
        .put('/admin/projects')
        .reply(400, {message: 'Test failure to edit project'});

      const expectedActions = [
        actions.setFailure('Failed to edit project', 'Test failure to edit project'),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.editProject(project)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
