export const Actions = {
  SET_INFORMATION: Symbol('SET_INFORMATION'),
  CHANGE_INFORMATION: Symbol('CHANGE_INFORMATION'),
};

const editInformation = (state= {information: {}, changed: {}}, action) => {
  switch (action.type) {
    case Actions.SET_INFORMATION:
      return {current: action.information, changed: action.information};
    case Actions.CHANGE_INFORMATION:
      return {...state, changed: {...state.changed, ...action.changes}};
    default:
      return state;
  }
};

export default editInformation;
