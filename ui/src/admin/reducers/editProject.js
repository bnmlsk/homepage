import _ from 'lodash';

export const Actions = {
  SET_PROJECTS: Symbol('SET_PROJECTS'),
  CHANGE_PROJECT: Symbol('CHANGE_PROJECT'),
  OPEN_MODAL: Symbol('OPEN_MODAL'),
  CLOSE_MODAL: Symbol('CLOSE_MODAL'),
};

const editProject = (state = {projects: []}, action) => {
  switch (action.type) {
    case Actions.SET_PROJECTS:
    return {...state, projects: action.projects};
  case Actions.CHANGE_PROJECT:
    return {...state, projectChanges: _.assign({}, state.projectChanges, action.changes)};
  case Actions.OPEN_MODAL:
    return {...state, projectChanges: action.project};
  case Actions.CLOSE_MODAL:
    return _.omit(state, 'projectChanges');
  default:
    return state;
  }
};

export default editProject;
