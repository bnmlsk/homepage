import _ from 'lodash';

export const Actions = {
  SET_ARTICLES: Symbol('SET_ARTICLES'),
  CHANGE_ARTICLE: Symbol('CHANGE_ARTICLE'),
  ADD_TAGS: Symbol('ADD_TAGS'),
  OPEN_MODAL: Symbol('OPEN_MODAL'),
  CLOSE_MODAL: Symbol('CLOSE_MODAL'),
};

const editArticle = (state = {articles: [], tags: []}, action) => {
  switch (action.type) {
    case Actions.SET_ARTICLES:
    return {...state,
      articles: action.articles,
      pageId: action.number + 1,
      totalPages: action.totalPages,
      tags: _.uniq(_.flatten(action.articles.map((article) => article.tags))),
    };
    case Actions.CHANGE_ARTICLE:
    return {...state, articleChanges: _.assign({}, state.articleChanges, action.changes)};
  case Actions.ADD_TAGS:
    return {...state, tags: [...action.tags, ...state.tags]};
    case Actions.OPEN_MODAL:
    return {...state, articleChanges: action.article};
  case Actions.CLOSE_MODAL:
    return _.omit(state, 'articleChanges');
  default:
    return state;
  }
};

export default editArticle;
