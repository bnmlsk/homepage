export const Actions = {
  SET_SUCCESS: Symbol('SET_SUCCESS'),
  DISMISS_SUCCESS: Symbol('DISMISS_SUCCESS'),
  SET_FAILURE: Symbol('SET_FAILURE'),
  DISMISS_FAILURE: Symbol('DISMISS_FAILURE'),
};

const message = (state = {}, action) => {
  switch (action.type) {
    case Actions.SET_SUCCESS:
    return {...state, success: {title: action.title}};
    case Actions.DISMISS_SUCCESS:
    return {...state, success: null};
    case Actions.SET_FAILURE:
    return {...state, failure: {title: action.title, message: action.message}};
    case Actions.DISMISS_FAILURE:
    return {...state, failure: null};
  default:
    return state;
  }
};

export default message;
