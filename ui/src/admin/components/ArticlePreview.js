import React from 'react';
import asHtml from 'react-html-parser';
import {Container, Divider, Header, Checkbox} from 'semantic-ui-react';
import Date from '../../Date';
import Tags from '../../Tags';

export default function ArticlePreview({article, editModal}) {
  return (
    <React.Fragment>
      <Header as='h2'>{article.title}</Header>
      <Date date={article.creationDate}/>
      <Tags tags={article.tags} />
      <Container>{asHtml(article.text)}</Container>
      <Divider/>
      <Checkbox disabled label='Published' checked={article.published}/>
      <Divider hidden />
      {editModal}
    </React.Fragment>
  );
}
