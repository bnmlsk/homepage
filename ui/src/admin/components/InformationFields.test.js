import React from 'react';
import {shallow} from 'enzyme';
import {Form, Message} from 'semantic-ui-react';
import InformationFields from './InformationFields';

describe('<InformationFields />', () => {
  const information = {
    realName: 'Test Name',
    name: 'nickname',
    email: 'test@email.com',
    shortInfo: 'Small info',
    fullInfo: 'More detailed information with <i>custom tags</i>.',

    bitbucket: 'bitbucket-link',
    github: 'github-link',
    linkedin: 'linkedin-link',
  };

  it('should display all information fields for edit', () => {
    const wrapper = shallow(<InformationFields information={information} />);

    expect(wrapper.contains(<Form.Input
      name='realName' label='Real name'
      placeholder='Real name' value='Test Name' />)).toBe(true);
    expect(wrapper.contains(<Form.Input
      name='name' label='Name'
      placeholder='Name' value='nickname' />)).toBe(true);
    expect(wrapper.contains(<Form.Input
      name='email' label='E-mail'
      placeholder='E-mail' value='test@email.com' />)).toBe(true);
    expect(wrapper.contains(<Form.TextArea
      name='shortInfo' label='Short info'
      placeholder='Short info' value='Small info' />)).toBe(true);
    expect(wrapper.contains(<Form.TextArea
      name='fullInfo' label='Full info' placeholder='Full info'
      value='More detailed information with <i>custom tags</i>.'/>)).toBe(true);
    expect(wrapper.contains(<Form.Input
      name='bitbucket' label='Bitbucket'
      placeholder='Bitbucket' value='bitbucket-link' />)).toBe(true);
    expect(wrapper.contains(<Form.Input
      name='github' label='GitHub'
      placeholder='GitHub' value='github-link' />)).toBe(true);
    expect(wrapper.contains(<Form.Input
      name='linkedin' label='LinkedIn'
      placeholder='LinkedIn' value='linkedin-link' />)).toBe(true);
  });

  it('should show success message if present', ()=> {
    const wrapper = shallow(<InformationFields information={information} success={{title: 'Test success'}} />);

    expect(wrapper.contains(<Message positive header='Test success' />)).toBe(true);
  });

  it('should show failure if present', () => {
    const failure = {title: 'Test failure', message: 'More detailed description'};

    const wrapper = shallow(<InformationFields information={information} failure={failure} />);

    expect(wrapper.contains(<Message negative header='Test failure' content='More detailed description' />)).toBe(true);
  });
});
