import React from 'react';
import {shallow} from 'enzyme';
import {Header, Container, Checkbox} from 'semantic-ui-react';
import ArticlePreview from './ArticlePreview';

describe('<ArticlePreview />', () => {
  it('should display article details', () => {
    const article = {
      title: 'Test article',
      link: 'article-test',
      text: 'Article description as <b>HTML</b> tags.',
      published: true,
    };

    const wrapper = shallow(<ArticlePreview article={article} />);

    expect(wrapper.contains(<Header as="h2">Test article</Header>)).toBe(true);
    expect(wrapper.contains(<Container>Article description as <b>HTML</b> tags.</Container>)).toBe(true);
    expect(wrapper.contains(<Checkbox disabled label='Published' checked={true} />)).toBe(true);
  });
});
