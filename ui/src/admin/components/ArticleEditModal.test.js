import React from 'react';
import {shallow} from 'enzyme';
import {Message, Form} from 'semantic-ui-react';
import ArticleEditModal from './ArticleEditModal';

describe('<ArticleEditModal />', () => {
  const labels = {icon: 'add', label: 'Test article modal', opened: true};

  it('should have tags as options for selection', () => {
    const tags = ['Java', 'React', 'Spring', 'Kubernetes'];

    const wrapper = shallow(<ArticleEditModal vis={labels} tags={tags} />);

    expect(wrapper.find(Form.Dropdown).props().options).toStrictEqual([
      {key: 'Java', text: 'Java', value: 'Java'},
      {key: 'React', text: 'React', value: 'React'},
      {key: 'Spring', text: 'Spring', value: 'Spring'},
      {key: 'Kubernetes', text: 'Kubernetes', value: 'Kubernetes'},
    ]);
  });

  it('should display failure message in case of error', () => {
    const failure = {title: 'Test failure', message: 'More detailed error description'};

    const wrapper = shallow(<ArticleEditModal vis={labels} failure={failure} tags={[]} />);

    expect(wrapper.contains(<Message negative header='Test failure' content='More detailed error description' />));
  });
});
