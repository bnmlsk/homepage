import React from 'react';
import {shallow} from 'enzyme';
import {Message} from 'semantic-ui-react';
import ProjectEditModal from './ProjectEditModal';

describe('<ProjectEditModal />', () => {
  const labels = {icon: 'add', label: 'Test project modal', opened: true};

  it('should display failure message in case of error', () => {
    const failure = {title: 'Test failure', message: 'More detailed error description'};

    const wrapper = shallow(<ProjectEditModal vis={labels} failure={failure} tags={[]} />);

    expect(wrapper.contains(<Message negative header='Test failure' content='More detailed error description' />));
  });
});
