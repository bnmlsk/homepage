import React from 'react';
import {shallow} from 'enzyme';
import {Header, Container} from 'semantic-ui-react';
import ProjectPreview from './ProjectPreview';

describe('<ProjectPreview />', () => {
  it('should display project details', () => {
    const project = {
      name: 'Test project',
      link: 'project-test',
      description: 'Project description as <b>HTML</b> tags.',
    };

    const wrapper = shallow(<ProjectPreview project={project} />);

    expect(wrapper.contains(<Header as="h2">Test project #project-test</Header>)).toBe(true);
    expect(wrapper.contains(<Container>Project description as <b>HTML</b> tags.</Container>)).toBe(true);
  });
});
