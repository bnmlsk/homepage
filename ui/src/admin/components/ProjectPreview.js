import React from 'react';
import {Container, Divider, Header, Segment} from 'semantic-ui-react';
import asHtml from 'react-html-parser';

export default function ProjectPreview({project, editModal}) {
  return (
    <React.Fragment>
      <Segment>
        <Header as="h2">{project.name} #{project.link}</Header>
        <Container>{asHtml(project.description)}</Container>
        <Divider hidden />
        {editModal}
      </Segment>
    </React.Fragment>
  );
}
