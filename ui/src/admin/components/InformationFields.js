import React from 'react';
import {Button, Form, Message} from 'semantic-ui-react';

export default function InformationFields({information,
                                           onChange,
                                           success,
                                           dismissSuccess,
                                           failure,
                                           dismissFailure,
                                           handleUpdate}) {
  return (
    <React.Fragment>
      {success && <Message positive header={success.title} onDismiss={dismissSuccess}/>}
      {failure && <Message negative header={failure.title} content={failure.message} onDismiss={dismissFailure}/>}
      <Form>
        <Form.Input
          name='realName'
          label='Real name'
          placeholder='Real name'
          value={information.realName || ''}
          onChange={onChange} />
        <Form.Input
          name='name'
          label='Name'
          placeholder='Name'
          value={information.name || ''}
          onChange={onChange} />
        <Form.Input
          name='email'
          label='E-mail'
          placeholder='E-mail'
          value={information.email || ''}
          onChange={onChange} />
        <Form.TextArea
          name='shortInfo'
          label='Short info'
          placeholder='Short info'
          value={information.shortInfo || ''}
          onChange={onChange} />
        <Form.TextArea
          name='fullInfo'
          label='Full info'
          placeholder='Full info'
          value={information.fullInfo || ''}
          onChange={onChange} />
        <Form.Input
          name='bitbucket'
          label='Bitbucket'
          placeholder='Bitbucket'
          value={information.bitbucket || ''}
          onChange={onChange} />
        <Form.Input
          name='github'
          label='GitHub'
          placeholder='GitHub'
          value={information.github || ''}
          onChange={onChange} />
        <Form.Input
          name='linkedin'
          label='LinkedIn'
          placeholder='LinkedIn'
          value={information.linkedin || ''}
          onChange={onChange} />
        <Button color='teal' onClick={handleUpdate}>Save</Button>
      </Form>
    </React.Fragment>
  );
}
