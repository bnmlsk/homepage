import React from 'react';
import {Icon, Button, Modal, Form, Message} from 'semantic-ui-react';

export default function ArticleEditModal({vis,
                                          article={},
                                          failure,
                                          tags,
                                          handleSubmit,
                                          handleNewTag,
                                          handleChange,
                                          handleOpened,
                                          dismissFailure}) {
  const failureMessage = failure && (
    <Message
      negative
      header={failure.title}
      content={failure.message}
      onDismiss={dismissFailure}/>);

  const options = tags ? tags.map((tag) => {
    return {key: tag, text: tag, value: tag};
  }) : [];

  return (
    <Modal
      trigger={
        <Button color='teal' onClick={() => handleOpened(true)}>
          <Icon name={vis.icon}/>
          {vis.label}
        </Button>}
      open={vis.opened}
      closeOnDimmerClick={false}>
      <Modal.Header>{vis.label}</Modal.Header>
      <Modal.Content>
        {failureMessage}
        <Form>
          <Form.Input name='title' label='Title' placeholder='Title' value={article.title} onChange={handleChange} />
          <Form.Input name='link' label='Link' placeholder='Link' value={article.link} onChange={handleChange} />
          <Form.TextArea name='text' label='Text' placeholder='Text' value={article.text} onChange={handleChange} />
          <Form.Dropdown
            name='tags' label='Tags' placeholder='Tags'
            fluid multiple search selection allowAdditions
            options={options} value={article.tags} onChange={handleChange} onAddItem={handleNewTag}/>
          <Form.Checkbox
            name='published' label='Published' checked={article.published}
            onChange={ (e, {name, checked}) => handleChange(e, {name: name, value: checked}) }/>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button content="Cancel" negative onClick={ () => handleOpened(false) }/>
        <Button content="Save" positive icon='checkmark' labelPosition='right' onClick={() => handleSubmit(article)}/>
      </Modal.Actions>
    </Modal>
  );
}
