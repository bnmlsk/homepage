import React from 'react';
import {Icon, Button, Modal, Form, Message} from 'semantic-ui-react';

export default function ProjectEditModal({vis,
                                          project={},
                                          failure,
                                          onSubmit,
                                          onChange,
                                          handleOpened,
                                          dismissFailure}) {
  return (
    <Modal
      trigger={
        <Button color='teal' onClick={() => handleOpened(true)}>
          <Icon name={vis.icon}/>
          {vis.label}
        </Button>}
      open={vis.opened}
      closeOnDimmerClick={false}>
      <Modal.Header>{vis.label}</Modal.Header>
      <Modal.Content>
        {failure && <Message negative header={failure.title} content={failure.message} onDismiss={dismissFailure}/>}
        <Form>
          <Form.Input name='name' label='Name' placeholder='Project name' value={project.name} onChange={onChange} />
          <Form.Input name='link' label='Link' placeholder='Link (anchor)' value={project.link} onChange={onChange} />
          <Form.TextArea
            name='description'
            label='Description'
            placeholder='Description (with HTML)'
            value={project.description}
            onChange={onChange} />
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button content="Cancel" negative onClick={() => handleOpened(false)}/>
        <Button content="Save" positive icon='checkmark' labelPosition='right' onClick={() => onSubmit(project)}/>
      </Modal.Actions>
    </Modal>
  );
}
