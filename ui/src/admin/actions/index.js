import {Actions as EditInformationActions} from '../reducers/editInformation';
import {Actions as EditArticleActions} from '../reducers/editArticle';
import {Actions as EditProjectActions} from '../reducers/editProject';
import {Actions as MessageActions} from '../reducers/message';

export const setInformation = (information) => ({
  type: EditInformationActions.SET_INFORMATION,
  information,
});

export const changeInformation = (changes) => ({
  type: EditInformationActions.CHANGE_INFORMATION,
  changes,
});

export const setArticles = (articles, number, totalPages, tag) => ({
  type: EditArticleActions.SET_ARTICLES,
  articles,
  number,
  totalPages,
});

export const changeArticle = (changes) => ({
  type: EditArticleActions.CHANGE_ARTICLE,
  changes,
});

export const addTags = (tags) => ({
  type: EditArticleActions.ADD_TAGS,
  tags,
});

export const openArticleModal = (article) => ({
  type: EditArticleActions.OPEN_MODAL,
  article,
});

export const closeArticleModal = () => ({
  type: EditArticleActions.CLOSE_MODAL,
});

export const setProjects = (projects) => ({
  type: EditProjectActions.SET_PROJECTS,
  projects,
});

export const changeProject = (changes) => ({
  type: EditProjectActions.CHANGE_PROJECT,
  changes,
});

export const openProjectModal = (project) => ({
  type: EditProjectActions.OPEN_MODAL,
  project,
});

export const closeProjectModal = () => ({
  type: EditProjectActions.CLOSE_MODAL,
});

export const setSuccess = (title) => ({
  type: MessageActions.SET_SUCCESS,
  title,
});

export const dismissSuccess = () => ({
  type: MessageActions.DISMISS_SUCCESS,
});

export const setFailure = (title, message) => ({
  type: MessageActions.SET_FAILURE,
  title,
  message,
});

export const dismissFailure = () => ({
  type: MessageActions.DISMISS_FAILURE,
});
