import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {useHistory} from 'react-router-dom';
import {push} from 'connected-react-router';
import {fetchProjects, findActiveIndexByHash} from './fetch';
import {setActiveProject} from '../actions';
import ProjectTabs from '../components/ProjectTabs';

export default function Projects() {
  const projects = useSelector((state) => state.projects);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(fetchProjects(history.location));
  }, [dispatch, history.location]);

  useEffect(() => {
    dispatch(setActiveProject(findActiveIndexByHash(projects.content, history.location)));
  }, [dispatch, projects.content, history.location]);

  const onProjectSelected = (activeIndex) => {
    dispatch(setActiveProject(activeIndex));
    dispatch(push(`${history.location.pathname}#${projects.content[activeIndex].link}`));
  };

  return (<ProjectTabs projects={projects} onProjectSelected={onProjectSelected}/>);
}
