import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Message} from 'semantic-ui-react';
import {Route, useHistory, useParams, useRouteMatch} from 'react-router-dom';
import {fetchArticlesByTag, fetchArticle} from './fetch';
import ArticlesPage from '../components/ArticlesPage';
import Article from '../components/Article';
import {useArticles} from "../reducers/articles";

export function AllArticles() {
  const articles = useArticles();
  const history = useHistory();

  const onPageChange = (pageId) => {
    history.push(`${history.location.pathname}?pageId=${pageId}`);
  };

  useEffect(() => {}, [history.location])

  return <ArticlesPage
      articles={articles.content}
      pageId={articles.pageId}
      totalPages={articles.totalPages}
      onPageChange={onPageChange} />
}

export function SearchArticles() {
  const articles = useSelector((state) => state.articles);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(fetchArticlesByTag(history.location));
  }, [dispatch, history.location]);

  const onPageChange = (pageId) => {
    history.push(`${history.location.pathname}?tag=${articles.tag}&pageId=${pageId}`);
  };

  return <React.Fragment>
      <Message>Search by tag <b>{articles.tag}</b></Message>
      <ArticlesPage
        articles={articles.content}
        pageId={articles.pageId}
        totalPages={articles.totalPages}
        onPageChange={onPageChange} />
    </React.Fragment>
}

export function SingleArticle() {
  const {link} = useParams();
  const article = useSelector((state) => state.articles.opened);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchArticle(link));
  }, [dispatch, link]);

  return <Article article={article} />;
}

export default function Articles() {
  const {path} = useRouteMatch();

  return <React.Fragment>
      <Route exact path={path}>
        <AllArticles />
      </Route>
      <Route path={`${path}/search`}>
        <SearchArticles />
      </Route>
      <Route path={`${path}/:link`}>
        <SingleArticle />
      </Route>
    </React.Fragment>
}
