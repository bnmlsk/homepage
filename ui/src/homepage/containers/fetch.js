import {
  setInformation,
  requestArticles,
  setArticles,
  openArticle,
  articleNotFound,
  setProjects,
} from '../actions';
import axios from 'axios';
import _ from 'lodash';
import queryString from 'query-string';

export function fetchInformation() {
  return async (dispatch) => {
    const response = await axios.get('/information');
    await dispatch(setInformation(response.data));
  }
}

export function fetchArticles(location) {
  return async (dispatch) => {
    dispatch(requestArticles(getPageId(location)));
    const response = await axios.get(`/articles${location.search}`);
    const articlesPage = response.data;
    dispatch(setArticles(articlesPage.content, articlesPage.number, articlesPage.totalPages));
  }
}

export function fetchArticle(link) {
  return async (dispatch) => {
    try {
      const response = await axios.get(`/articles/${link}`);
      await dispatch(openArticle(response.data));
    } catch(error) {
      dispatch(articleNotFound(link));
    }
  }
}

export function fetchArticlesByTag(location) {
  return async (dispatch) => {
    dispatch(requestArticles(getPageId(location), getTag(location)));
    const response = await axios.get(`/articles${location.search}`);
    const articlesPage = response.data;
    const tag = queryString.parse(location.search).tag;
    dispatch(setArticles(articlesPage.content, articlesPage.number, articlesPage.totalPages, tag));
  }
}

export function fetchProjects(location) {
  return async (dispatch) => {
    const response = await axios.get('/projects');

    const projects = response.data;

    dispatch(setProjects(projects, findActiveIndexByHash(projects, location)));
  }
}

function getPageId(location) {
  return queryString.parse(location.search).pageId;
}
function getTag(location) {
  return queryString.parse(location.search).tag;
}

export function findActiveIndexByHash(projects, location) {
  const link = _.trimStart(location.hash, '#');
  return _.isEmpty(link) ? 0 : _.findIndex(projects, {link});
}
