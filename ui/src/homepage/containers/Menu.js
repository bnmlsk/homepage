import React, {useCallback, useEffect} from 'react';
import {Pages, choosePage} from '../actions';
import {useLocation} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import Header from '../components/Header';
import {useIsAuthorised} from "../../login/reducers/login";

const _ = require('lodash');

export default function Menu() {

  const currentPage = useSelector((state) => state.currentPage);
  const isAuthorised = useIsAuthorised();

  const dispatch = useDispatch();
  const location = useLocation();

  const getActiveItem = useCallback(function () {
    const [, root] = _.split(location.pathname, '/', 2);
    switch (root) {
      case 'articles':
        return Pages.ARTICLES;
      case 'projects':
        return Pages.PROJECTS;
      default:
        return Pages.INFORMATION;
    }
  }, [location.pathname]);

  useEffect(() => {
    dispatch(choosePage(getActiveItem()));
  }, [dispatch, getActiveItem, location.pathname, isAuthorised]);

  return ( <Header activeItem={currentPage} isAuthorised={isAuthorised} /> );
}
