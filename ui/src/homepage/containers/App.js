import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {Container, Grid, Segment} from 'semantic-ui-react';
import FullInformation from '../components/FullInformation';
import Information from '../components/Information';
import Footer from '../components/Footer';
import Menu from './Menu';
import Articles from './Articles';
import Projects from './Projects';
import {useInformation} from '../reducers/information';
import './App.css';

export default function App() {
  const information = useInformation();

  return <div className="flex main">
      <Menu />
      <Container className="flex container">
        <Grid stackable>
          <Grid.Column width={10}>
            <Segment raised>
              <Switch>
                <Route path="/articles" component={Articles}/>
                <Route path="/projects" component={Projects} />
                <Route path="/">
                  <FullInformation information={information} />
                </Route>
              </Switch>
            </Segment>
          </Grid.Column>
          <Grid.Column width={5}>
            <Information information={information} />
          </Grid.Column>
        </Grid>
      </Container>
      <Footer information={information} />
    </div>;
}
