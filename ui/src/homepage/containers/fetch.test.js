import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import expect from 'expect';

import * as actions from '../actions';
import * as fetchers from './fetch';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('fetch', () => {
  describe('fetchInformation', () => {
    it('should fetch information and send it as a state', () => {
      const information = {
        fullName: 'Test Name',
        shortInfo: 'Test short information',
      };

      nock('http://localhost:8080')
        .get('/information')
        .reply(200, information);

      const expectedActions = [
        actions.setInformation(information),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.fetchInformation()).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchArticle', () => {
    it('should set an opened article by link', () => {
      const article = {
        title: 'Test article',
        link: 'article-link',
        published: true,
      };

      nock('http://localhost:8080')
        .get('/articles/article-link')
        .reply(200, article);

      const expectedActions = [
        actions.openArticle(article),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.fetchArticle('article-link')).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should set article not found if could not fetch it by link', () => {
      nock('http://localhost:8080')
        .get('/articles/article-link')
        .reply(404);

      const expectedActions = [
        actions.articleNotFound('article-link'),
      ];

      const store = mockStore({ });

      return store.dispatch(fetchers.fetchArticle('article-link')).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchArticles', () => {
    it('should set fetched articles', () => {
      const articles = [
        {title: 'Article 1', link: 'article-1', tags: ['Java']},
        {title: 'Article 2', link: 'article-2', tags: ['Java']},
        {title: 'Article 3', link: 'article-3', tags: ['Java']},
      ];
      const pageId = 2;
      const totalPages = 10;

      nock('http://localhost:8080')
        .get('/articles?pageId=2')
        .reply(200, {
          content: articles,
          number: pageId,
          totalPages: totalPages,
        });

      const expectedActions = [
        actions.setArticles(articles, pageId, totalPages),
      ];

      const store = mockStore({ });

      const location = {search: '?pageId=2'};
      return store.dispatch(fetchers.fetchArticles(location)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchArticlesByTag', () => {
    it('should set fetched articles by tag', () => {
      const articles = [
        {title: 'Article 1', link: 'article-1', tags: ['Java']},
        {title: 'Article 2', link: 'article-2', tags: ['Java']},
        {title: 'Article 3', link: 'article-3', tags: ['Java']},
      ];
      const pageId = 2;
      const totalPages = 10;
      const tag = 'Java';

      nock('http://localhost:8080')
        .get('/articles?pageId=2&tag=Java')
        .reply(200, {
          content: articles,
          number: pageId,
          totalPages: totalPages,
        });

      const expectedActions = [
        actions.setArticles(articles, pageId, totalPages, tag),
      ];

      const store = mockStore({ });

      const location = {search: '?pageId=2&tag=Java'};
      return store.dispatch(fetchers.fetchArticlesByTag(location)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });

  describe('fetchProjects', () => {
    it('should set all fetched projects and find active by anchor value in location', () => {
      const projects = [
        {name: 'Project 1', link: 'project-1'},
        {name: 'Project 2', link: 'project-2'},
        {name: 'Project 3', link: 'project-3'},
        {name: 'Project 4', link: 'project-4'},
      ];

      nock('http://localhost:8080')
        .get('/projects')
        .reply(200, projects);

      const expectedActions = [
        actions.setProjects(projects, 2),
      ];

      const store = mockStore({ });
      const location = {hash: '#project-3'};

      return store.dispatch(fetchers.fetchProjects(location)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });

    it('should set all fetched projects and active aas 0 if no anchor is set', () => {
      const projects = [
        {name: 'Project 1', link: 'project-1'},
        {name: 'Project 2', link: 'project-2'},
        {name: 'Project 3', link: 'project-3'},
        {name: 'Project 4', link: 'project-4'},
      ];

      nock('http://localhost:8080')
        .get('/projects')
        .reply(200, projects);

      const expectedActions = [
        actions.setProjects(projects, 0), // no hash in location
      ];

      const store = mockStore({ });
      const location = { };

      return store.dispatch(fetchers.fetchProjects(location)).then(() => {
        expect(store.getActions()).toEqual(expectedActions);
      });
    });
  });
});
