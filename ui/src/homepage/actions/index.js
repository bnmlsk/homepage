import {Actions as InformationActions} from '../reducers/information';
import {Actions as CurrentPageActions} from '../reducers/currentPage';
import {Actions as ArticlesActions} from '../reducers/articles';
import {Actions as ProjectsActions} from '../reducers/projects';

export const choosePage = (page) => ({
  type: CurrentPageActions.CHOOSE_PAGE,
  page,
});

export const Pages = {
  INFORMATION: 'INFORMATION',
  ARTICLES: 'ARTICLES',
  PROJECTS: 'PROJECTS',
};

export const setInformation = (information) => ({
  type: InformationActions.SET_INFORMATION,
  information,
});

export const requestArticles = (pageId, tag = null) => ({
  type: ArticlesActions.REQUEST_ARTICLES,
  pageId,
  tag,
});

export const setArticles = (articles, number, totalPages, tag= null) => ({
  type: ArticlesActions.SET_ARTICLES,
  articles,
  number,
  totalPages,
  tag,
});

export const openArticle = (opened) => ({
  type: ArticlesActions.OPEN_ARTICLE,
  opened,
});

export const articleNotFound = () => ({
  type: ArticlesActions.ARTICLE_NOT_FOUND,
});

export const setProjects = (projects, activeIndex) => ({
  type: ProjectsActions.SET_PROJECTS,
  projects,
  activeIndex,
});

export const setActiveProject = (activeIndex) => ({
  type: ProjectsActions.SET_ACTIVE_PROJECT,
  activeIndex,
});