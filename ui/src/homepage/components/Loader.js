import {Placeholder, Segment} from "semantic-ui-react";
import React from "react";
import {times} from "lodash";

export default function Loader({lines = 5}) {
  return <Segment>
    <Placeholder>
      <Placeholder.Paragraph>
        {times(lines, () => <Placeholder.Line />)}
      </Placeholder.Paragraph>
    </Placeholder>
  </Segment>;
}