import React from 'react';
import {Pages} from '../actions';
import {Link} from 'react-router-dom';
import {Menu, Container, Button} from 'semantic-ui-react'

export default function Header({activeItem, isAuthorised}) {

  const adminLink = <Link to="/admin">
    <Menu.Item name='admin'>
      <Button primary>
        Admin
      </Button>
    </Menu.Item>
  </Link>;

  return (
    <Menu as='nav' inverted borderless stackable>
      <Container>
        <Link to="/">
          <Menu.Item name='home' active={activeItem === Pages.INFORMATION}>Home</Menu.Item>
        </Link>

        <Link to="/articles">
          <Menu.Item name='articles' active={activeItem === Pages.ARTICLES}>Articles</Menu.Item>
        </Link>

        <Link to="/projects">
          <Menu.Item name='projects' active={activeItem === Pages.PROJECTS}>Projects</Menu.Item>
        </Link>

        {isAuthorised && adminLink}
      </Container>
    </Menu>
  );
}
