import React from 'react';
import {shallow} from 'enzyme';
import sinon from 'sinon';
import {Pagination} from 'semantic-ui-react';
import ArticlesPage from './ArticlesPage';
import Article from './Article';

describe('<ArticlesPage/>', () => {
  it('should display all articles', () => {
    const articles = [
      {link: 'article-1'},
      {link: 'article-2'},
      {link: 'article-3'},
    ];

    const wrapper = shallow(<ArticlesPage articles={articles} pageId={1} totalPages={10}/>);

    expect(wrapper.find(Article)).toHaveLength(3);

    const pagination = wrapper.find(Pagination);

    expect(pagination.props().activePage).toBe(1);
    expect(pagination.props().totalPages).toBe(10);
    expect(pagination.props().boundaryRange).toBe(5);
  });

  it('should propagate page change', () => {
    const onPageChange = sinon.spy();
    const wrapper = shallow(<ArticlesPage articles={[]} pageId={1} totalPages={10} onPageChange={onPageChange}/>);

    wrapper.find('Pagination').props().onPageChange(null, {activePage: 2});

    expect(onPageChange).toHaveProperty('callCount', 1);
  });
});
