import React from 'react';
import {shallow} from 'enzyme';
import Header from './Header';
import {Pages} from '../actions';

describe('<Header />', () => {
  it('should have Home item active if page is INFORMATION', () => {
    const wrapper = shallow(<Header activeItem={Pages.INFORMATION}/>);

    expectActiveItemIs(wrapper, 'home');
  });

  it('should have Articles item active if page is ARTICLES', () => {
    const wrapper = shallow(<Header activeItem={Pages.ARTICLES}/>);

    expectActiveItemIs(wrapper, 'articles');
  });

  it('should have Projects item active if page is PROJECTS', () => {
    const wrapper = shallow(<Header activeItem={Pages.PROJECTS}/>);

    expectActiveItemIs(wrapper, 'projects');
  });

  const expectActiveItemIs = (wrapper, name) => {
    const activeItem = wrapper.find({active: true});
    expect(activeItem).toHaveLength(1);
    expect(activeItem.props().name).toBe(name);
  };
});
