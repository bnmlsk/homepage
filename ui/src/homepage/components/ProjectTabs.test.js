import React from 'react';
import {shallow} from 'enzyme';
import {Tab, Message} from 'semantic-ui-react';
import ProjectTabs from './ProjectTabs';

describe('<ProjectTabs />', () => {
  const projects = {
    content: [
      {name: 'Project 1', description: 'Test project <b>1</b>', link: 'project-1'},
      {name: 'Project 2', description: 'Test project <b>2</b>', link: 'project-2'},
      {name: 'Project 3', description: 'Test project <b>3</b>', link: 'project-3'},
      {name: 'Project 4', description: 'Test project <b>4</b>', link: 'project-4'},
    ],
    activeIndex: 2,
  };

  it('should create a tab per project', () => {
    const wrapper = shallow(<ProjectTabs projects={projects} />);

    expect(wrapper.find(Tab).props().panes).toHaveLength(4);
  });

  it('should open a tab with active project', () => {
    const wrapper = shallow(<ProjectTabs projects={projects} />);

    expect(wrapper.find(Tab).props().activeIndex).toBe(2);
    expect(wrapper.contains(<Message/>)).toBe(false);
  });

  it('should show an error if opened non-existent project', () => {
    const nonExistentActiveProject = {content: [], activeIndex: -1};

    const wrapper = shallow(<ProjectTabs projects={nonExistentActiveProject} onProjectSelected={() => {}} />);

    expect(wrapper.contains(<Message negative header='Link to unknown project'/>)).toBe(true);
  });
});
