import React from 'react';
import {Link} from 'react-router-dom';
import {Segment, Header, Container} from 'semantic-ui-react';
import asHtml from 'react-html-parser';
import Tags from '../../Tags';
import Date from '../../Date';

export default function Article({article}) {
  if (article === 'Not found') {
    return <Segment raised>No such article exists</Segment>;
  }

  if (!article) {
    return <></>;
  }

  return <Segment raised>
      <Link to={`/articles/${article.link}`}>
        <Header as='h2'>{article.title}</Header>
      </Link>
      <Date date={article.creationDate}/>
      <Tags tags={article.tags} />
      <Container>{asHtml(article.text)}</Container>
    </Segment>
}
