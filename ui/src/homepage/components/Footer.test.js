import React from 'react';
import {shallow} from 'enzyme';
import Footer from './Footer';

describe('<Footer />', () => {
  const information = {
    bitbucket: 'bitbucket-link',
    github: 'github-link',
    linkedin: 'linkedin-link',
    email: 'test@email.com',
  };

  it('should display links to social media', () => {
    const wrapper = shallow(<Footer information={information}/>);

    expect(wrapper.find('#bitbucket').props().href).toBe('bitbucket-link');
    expect(wrapper.find('#github').props().href).toBe('github-link');
    expect(wrapper.find('#linkedin').props().href).toBe('linkedin-link');
  });
});
