import React from 'react';
import {shallow} from 'enzyme';
import {Link} from 'react-router-dom';
import {Segment, Header, Container} from 'semantic-ui-react';
import Article from './Article';
import Date from '../../Date';
import Tags from '../../Tags';

describe('Single article open', () => {
  it('should render an article if found', () => {
    const article = {
      title: 'Test Article',
      link: 'article-1',
      creationDate: '2019-10-06',
      tags: ['Java', 'Spring'],
      text: '<p>Simple text with <b>bold</b></p>',
    };

    const wrapper = shallow(<Article article={article}/>);

    expect(wrapper.contains(<Link to='/articles/article-1'><Header as='h2'>Test Article</Header></Link>)).toBe(true);
    expect(wrapper.contains(<Date date='2019-10-06'/>)).toBe(true);
    expect(wrapper.contains(<Tags tags={['Java', 'Spring']}/>)).toBe(true);
    expect(wrapper.contains(<Container><p>Simple text with <b>bold</b></p></Container>)).toBe(true);
  });

  it('should show an error if article does not exist', () => {
    const article = 'Not found';

    const wrapper = shallow(<Article article={article}/>);

    expect(wrapper.contains(<Segment raised>No such article exists.</Segment>)).toBe(true);
  });
});
