import React from 'react';
import {Icon, Card} from 'semantic-ui-react';
import {isEmpty} from "lodash";
import Loader from "./Loader";

export default function Information({information}) {
  if (isEmpty(information)) {
    return <Loader />;
  }

  return <Card>
      <Card.Content>
        <Card.Header>{information.realName}</Card.Header>
        <Card.Meta>{information.name}</Card.Meta>
        <Card.Description>{information.shortInfo}</Card.Description>
      </Card.Content>
      <Card.Content extra>
        <a href={'mailto:' + information.email}>
          <Icon link name='mail outline' />
          {information.email}
        </a>
      </Card.Content>
    </Card>
}
