import React from 'react';
import {Pagination} from 'semantic-ui-react';
import Article from './Article';
import Loader from "./Loader";

export default function ArticlesPage({articles, pageId, totalPages, onPageChange}) {

  if (!articles) {
    return <Loader />;
  }

  return <React.Fragment>
      {articles.map((article) => <Article key={article.link} article={article} />)}
      <Pagination
        activePage={pageId}
        totalPages={totalPages}
        boundaryRange={5}
        onPageChange={ (event, data) => onPageChange(data.activePage) } />
    </React.Fragment>;
}
