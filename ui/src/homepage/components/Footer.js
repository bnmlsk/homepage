import React from 'react';
import {Icon, Container, Header, Grid, Segment, List} from 'semantic-ui-react';

export default function Footer({information}) {
  return (
    <Segment inverted vertical as="footer" style={{marginTop: 20}}>
      <Container>
        <Grid stackable inverted divided>
          <Grid.Column width={3}>
            <Header as='h4' inverted>Menu</Header>
            <List inverted link>
              <List.Item as='a' link='#top'>Back to top</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={5}>
            <Header as='h4' inverted>License</Header>
            <p>This program is free software; you can redistribute it and/or modify it under the terms
                            of the GNU General Public License as published by the Free Software Foundation; either
                            version 2 of the License, or (at your option) any later version.</p>
          </Grid.Column>
          <Grid.Column width={5}>
            <Container as='div'>
              <a id="bitbucket" href={information.bitbucket}>
                <Icon link name='bitbucket' size="large" inverted />
              </a>
              <a id="github" href={information.github}>
                <Icon link name='github' size="large" inverted />
              </a>
              <a id="linkedin" href={information.linkedin}>
                <Icon link name='linkedin' size="large" inverted />
              </a>
              <a href="/rssfeed">
                <Icon link name='rss' size="large" inverted />
              </a>
            </Container>
            <Header as='h4' inverted>Notice</Header>
            <Container as='p'>
                If you have any troubles visiting this site, please write to me about it on <a href={'mailto:' + information.email}>{information.email}</a>
            </Container>
          </Grid.Column>
        </Grid>
      </Container>
    </Segment>
  );
}
