import React from 'react';
import {shallow} from 'enzyme';
import {Container} from 'semantic-ui-react';
import FullInformation from './FullInformation';

describe('<FullInformation />', () => {
  const information = {
    // described as HTML block
    fullInfo: '<p>This test information is <i>HTML</i> block.</p>',
  };

  it('should display information as html block', () => {
    const wrapper = shallow(<FullInformation information={information}/>);

    expect(wrapper.contains(<Container><p>This test information is <i>HTML</i> block.</p></Container>)).toBe(true);
  });
});
