import React from 'react';
import {Container} from 'semantic-ui-react';
import asHtml from 'react-html-parser';
import {isEmpty} from "lodash";
import Loader from "./Loader";

export default function FullInformation({information}) {
  if (isEmpty(information)) {
    return <Loader />
  }

  return <Container>
    {asHtml(information.fullInfo)}
  </Container>;
}
