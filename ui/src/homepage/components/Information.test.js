import React from 'react';
import {shallow} from 'enzyme';
import {Card} from 'semantic-ui-react';
import Information from './Information';

describe('<Information />', () => {
  const information = {
    realName: 'John Smith',
    name: 'nickname',
    shortInfo: 'Small information about me',
  };

  it('should display information details', () => {
    const wrapper = shallow(<Information information={information} />);

    expect(wrapper.contains(<Card.Header>John Smith</Card.Header>)).toBe(true);
    expect(wrapper.contains(<Card.Meta>nickname</Card.Meta>)).toBe(true);
    expect(wrapper.contains(<Card.Description>Small information about me</Card.Description>)).toBe(true);
  });
});
