import React from 'react';
import asHtml from 'react-html-parser';
import {Tab, Message} from 'semantic-ui-react';
import Loader from "./Loader";

export default function ProjectTabs({projects, onProjectSelected}) {

  if (!projects.content) {
    return <Loader />
  }

  const panes = projects.content.map((project) => {
    return {
      menuItem: project.name,
      render: () => <Tab.Pane>{asHtml(project.description)}</Tab.Pane>,
    };
  });

  return (
    <React.Fragment>
      {projects.activeIndex === -1 && <Message negative header='Link to unknown project' />}
      <Tab
        panes={panes}
        activeIndex={projects.activeIndex}
        onTabChange={(event, data) => onProjectSelected(data.activeIndex)}/>
    </React.Fragment>
  );
}
