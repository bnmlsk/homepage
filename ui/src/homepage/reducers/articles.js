import {useDispatch, useSelector} from "react-redux";
import {useLocation} from "react-router-dom";
import {useEffect} from "react";
import {fetchArticles} from "../containers/fetch";

export const Actions = {
  REQUEST_ARTICLES: Symbol('REQUEST_ARTICLES'),
  SET_ARTICLES: Symbol('SET_ARTICLES'),
  OPEN_ARTICLE: Symbol('OPEN_ARTICLE'),
  ARTICLE_NOT_FOUND: Symbol('ARTICLE_NOT_FOUND'),
};

const articles = (state = { content: null, pageId: null, totalPages: 0, tag: null, opened: null}, action) => {
  switch (action.type) {
    case Actions.REQUEST_ARTICLES:
      return {
        ...state,
        content: undefined,
        pageId: action.pageId,
        tag: action.tag,
      };
    case Actions.SET_ARTICLES:
      return {
        ...state,
        content: action.articles,
        pageId: action.number + 1,
        totalPages: action.totalPages,
        tag: action.tag,
        opened: null
      };
    case Actions.OPEN_ARTICLE:
    return {...state,
      opened: action.opened};
    case Symbol.ARTICLE_NOT_FOUND:
    return {...state,
      opened: 'Not found'};
  default:
    return state;
  }
};

export function useArticles() {

  const dispatch = useDispatch();
  const location = useLocation();
  const articles = useSelector((state) => state.articles);

  useEffect(() => {
      dispatch(fetchArticles(location));
  }, [dispatch, location]);

  return articles;
}

export default articles;
