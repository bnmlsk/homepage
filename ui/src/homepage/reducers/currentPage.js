import {Pages} from '../actions';

export const Actions = {
  CHOOSE_PAGE: Symbol('CHOOSE_PAGE'),
}

const currentPage = (state = Pages.INFORMATION, action) => {
  switch (action.type) {
    case Actions.CHOOSE_PAGE:
      return action.page;
  default:
    return state;
  }
};

export default currentPage;
