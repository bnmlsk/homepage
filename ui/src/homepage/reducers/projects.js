export const Actions = {
  SET_PROJECTS: Symbol('SET_PROJECTS'),
  SET_ACTIVE_PROJECT: Symbol('SET_ACTIVE_PROJECT'),
};

const projects = (state = {content: null, activeIndex: 0}, action) => {
  switch (action.type) {
  case Actions.SET_PROJECTS:
    return {content: action.projects, activeIndex: action.activeIndex};
    case Actions.SET_ACTIVE_PROJECT:
    return {...state, activeIndex: action.activeIndex};
  default:
    return state;
  }
};

export default projects;
