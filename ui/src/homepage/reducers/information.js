import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {fetchInformation} from "../containers/fetch";
import {isEmpty} from "lodash";

export const Actions = {
  SET_INFORMATION: Symbol('SET_INFORMATION'),
}

const information = (state = {}, action) => {
  switch (action.type) {
  case Actions.SET_INFORMATION:
    return action.information;
  default:
    return state;
  }
};

export function useInformation() {
  const dispatch = useDispatch();
  const information = useSelector((state) => state.information);

  useEffect(() => {
    if (isEmpty(information)) {
      dispatch(fetchInformation());
    }
  }, [dispatch, information]);

  return information;
}

export default information;
