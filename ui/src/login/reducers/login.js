import moment from 'moment';
import {useDispatch, useSelector} from "react-redux";
import {useEffect} from "react";
import {loggedIn} from "../actions";

export const Actions = {
  LOGGING_IN: Symbol('LOGGING_IN'),
  LOGGED: Symbol('LOGGED'),
  LOGGED_OFF: Symbol('LOGGED_OFF'),
};

export const State = {
  IN_PROGRESS: Symbol('IN_PROGRESS'),
  LOGGED_IN: Symbol('LOGGED_IN'),
  UNKNOWN: Symbol('UNKNOWN')
}

const login = (state = { state: State.UNKNOWN }, action) => {
  switch(action.type) {
    case Actions.LOGGING_IN:
      return {...state, username: action.username, state: State.IN_PROGRESS };
    case Actions.LOGGED:
      const expires = moment().add(30, "minutes");
      window.localStorage.setItem("username", action.username);
      window.localStorage.setItem("expires", expires);
      return {...state, username: action.username, expires: expires, state: State.LOGGED_IN };
    case Actions.LOGGED_OFF:
      window.localStorage.removeItem("username");
      window.localStorage.removeItem("expires");
      return { state: State.UNKNOWN };
    default:
      return state;
  }
}

export function useIsAuthorised() {

  const login = useSelector((state) => state.login);
  const dispatch = useDispatch();

  useEffect(() => {
    const storedUsername = window.localStorage.getItem("username");
    const storedExpires = window.localStorage.getItem("expires");

    if (login.state !== State.LOGGED_IN && storedUsername && moment().isBefore(storedExpires)) {
      dispatch(loggedIn(storedUsername, storedExpires));
    }
  }, [dispatch, login]);

  return login.state === State.LOGGED_IN && moment().isBefore(login.expires);
}

export default login;