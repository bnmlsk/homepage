import {Actions} from '../reducers/login'

export const startLogin = (username) => ({
  type: Actions.LOGGING_IN,
  username
})

export const loggedIn = (username, expires) => ({
  type: Actions.LOGGED,
  username,
  expires
})

export const loggedOff = () => ({
  type: Actions.LOGGED_OFF,
})