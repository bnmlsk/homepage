import React, {useEffect} from "react";
import {Container, Form, Icon, Message} from "semantic-ui-react";
import {useDispatch} from "react-redux";
import {push} from "connected-react-router";
import {useState} from "react";
import {doLogin} from "./fetch";
import {loggedIn, loggedOff, startLogin} from "../actions";
import moment from "moment";

export default function Login() {

  const dispatch = useDispatch();

  const [username, setUsername] = useState();
  const [password, setPassword] = useState();
  const [inProgress, setInProgress] = useState(false);

  const [errorMessage, setErrorMessage] = useState(null);

  useEffect(() => {}, [inProgress]);

  let submit = async () => {
    try {
      await setInProgress(true);
      await dispatch(startLogin(username));
      await dispatch(doLogin(username, password));
      await dispatch(loggedIn(username, moment().add(10, 'minutes')));
      await dispatch(push("/admin"));
    } catch (e) {
      await dispatch(loggedOff());
      setErrorMessage('Login failed' + e);
    } finally {
      setInProgress(false);
    }
  };

  return <Container>
    <Form error={!!errorMessage} loading={inProgress}>
      <Form.Input label="Username" value={username} autoComplete="username" onChange={(e, {value}) => setUsername(value)}/>
      <Form.Input type="password" label="Password" value={password} autoComplete="current-password" onChange={(e, {value}) => setPassword(value)} />
      <Message error>
        <Message.Header>Error</Message.Header>
        <Message.Content>{errorMessage}</Message.Content>
      </Message>
      <Form.Button primary onClick={submit}><Icon name="sign-in"/>Login</Form.Button>
    </Form>
  </Container>
}