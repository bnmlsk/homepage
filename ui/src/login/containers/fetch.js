import axios from "axios";

export function doLogin(username, password) {
  return async (dispatch) => {
    let formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);
    await axios.post('/login', formData);
  }
}