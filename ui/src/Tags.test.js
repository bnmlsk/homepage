import React from 'react';
import {shallow} from 'enzyme';
import Tags from './Tags';
import {Link} from 'react-router-dom';

describe('<Tags/>', () => {
  it('should display all tags', () => {
    const tags = ['Java', 'Spring', 'Kubernetes'];

    const wrapper = shallow(<Tags tags={tags} />);

    const links = wrapper.find(Link);
    expect(links).toHaveLength(3);
    expect(links.at(0).props().to).toBe('/articles/search?tag=Java');
    expect(links.at(1).props().to).toBe('/articles/search?tag=Spring');
    expect(links.at(2).props().to).toBe('/articles/search?tag=Kubernetes');
  });

  it('should display no tags if null', () => {
    const wrapper = shallow(<Tags tags={null}/>);

    expect(wrapper.find(Link)).toHaveLength(0);
  });
});
